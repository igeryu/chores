package org.magipixel.chores;

import org.magipixel.chores.error.EmailExistsException;
import org.magipixel.chores.model.User;
import org.magipixel.chores.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Controller
public class LoginController {

    @Autowired
    @NotNull
    UserService service;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage(@RequestParam Map<String, String> map, final Model model) {
        if(map.get("error") != null) {
            model.addAttribute("error", "true");
        }

        if(map.get("logout") != null) {
            model.addAttribute("logout", "true");
        }
        model.addAttribute("title", "Login");
        return "login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegistrationPage(final Model model) {
        model.addAttribute("title", "Register new user");
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUserAccount(@RequestParam Map<String, String> map, final Model model) {
        final String password = map.get("password");
        final String matchingPassword = map.get("matchingPassword");
        if (password.equals(matchingPassword)) {
            final UserDto userDto = new UserDto();
            userDto.setFirstName(map.get("firstname"));
            userDto.setLastName(map.get("lastname"));
            final String email = map.get("email");
            userDto.setEmail(email);
            userDto.setPassword(password);
            userDto.setMatchingPassword(matchingPassword);
            User registeredUser = registerUserAccount(userDto);
            if (registeredUser != null) {
                return "main";
            } else {
                String message = "Email '" + email + "' already registered!";
                model.addAttribute("error", message);
            }
        } else {
            model.addAttribute("error", "Passwords don't match");
        }
        return "register";
    }

    private User registerUserAccount(final UserDto userDto) {
        User registered = null;
        try {
            registered = service.registerNewUserAccount(userDto);
        } catch (EmailExistsException ex) {
            return null;
        }

        return registered;
    }
}
