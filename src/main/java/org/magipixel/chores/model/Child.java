package org.magipixel.chores.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = " children")
public class Child {
    @Id
    private String id;

    @Indexed
    private String name;
}
