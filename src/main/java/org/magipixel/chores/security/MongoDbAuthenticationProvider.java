package org.magipixel.chores.security;

import org.magipixel.chores.model.User;
import org.magipixel.chores.repo.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class MongoDbAuthenticationProvider implements AuthenticationProvider {
    private final Logger logger = LoggerFactory.getLogger(MongoDbAuthenticationProvider.class);

    @Autowired @NotNull UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String email = authentication.getName();
        logger.debug("Authenticating user with email: " + email);
        final Object credentials = authentication.getCredentials();
        if (!(credentials instanceof String)) {
            logger.info("Provided password is not a string value");
            return null;
        }

        final String password = credentials.toString();
        final User user = userRepository.findByEmail(email);

        if (user == null
                || user.getPassword() == null
                || !user.getPassword().equals(password)) {
            logger.debug((user == null ? "No user: " : "bad password: ") + email);
            throw new BadCredentialsException("Authentication failed for " + email);
        }
        logger.info("User '{}' logged in successfully", email);

        final List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()));
        final Authentication auth = new
                UsernamePasswordAuthenticationToken(email, password, grantedAuthorities);
        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
