package org.magipixel.chores.repo;

import org.magipixel.chores.model.Child;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChildRepository extends MongoRepository<Child, String> {
    Child findByName(final String name);
}
