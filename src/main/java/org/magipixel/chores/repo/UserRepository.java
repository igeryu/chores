package org.magipixel.chores.repo;

import org.magipixel.chores.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public interface UserRepository extends MongoRepository<User, String> {
    User findByEmail(final String email);

}
