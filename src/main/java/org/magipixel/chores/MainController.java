package org.magipixel.chores;

import org.magipixel.chores.model.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class MainController {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ChildService childService;

    @RequestMapping("/index")
    public ModelAndView index(Map<String, Object> model) {
        model.put("title", "Chores App - MagiPixel");
        model.put("message", "Work in Progress");
        return new ModelAndView("index", model);
    }

    @RequestMapping("/web/test")
    public ModelAndView test(Map<String, Object> model) {
        model.put("title", "Chores App - MagiPixel");
        model.put("message", "Work in Progress");
        return new ModelAndView("mustacheTest", model);
    }

    @RequestMapping(value = "/addChild", method = RequestMethod.GET)
    public @ResponseBody
    Child addChild(@RequestParam("name") final String name) {
        final Child newChild = new Child();
        newChild.setName(name);
        mongoTemplate.insert(newChild, "child");
        return childService.addChild(newChild);
    }
}
