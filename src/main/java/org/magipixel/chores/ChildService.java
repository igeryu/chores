package org.magipixel.chores;

import org.magipixel.chores.model.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChildService {

//    @Autowired
//    private ChildRepository childRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    /**
     * {@inheritDoc}
     */

//    public void addChild(Child child) {
    // FIXME
    public Child addChild(Child child) {
        final List<Child> results = mongoTemplate.findAll(Child.class, "child");
//        Child dbChild = childRepository.findByName(child.getName());

//        if (dbChild != null) {
        if (results != null && !results.isEmpty()) {

//            throw new JusticeLeagueManagementException(ErrorMessages.MEMBER_ALREDY_EXISTS);

        }

//        childRepository.insert(child, "child");
        mongoTemplate.insert(child, "childTest");
        return child;
    }
}