package org.magipixel.chores;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertTrue;

public class MainControllerTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    @Ignore
    public void givenIndexPage_whenContainsArticle_thenTrue() {

        ResponseEntity<String> entity
                = this.restTemplate.getForEntity("/tes", String.class);

        assertTrue(entity.getStatusCode()
                .equals(HttpStatus.OK));
//        assertTrue(entity.getBody()
//                .contains("Article Title 0"));
    }

}
